# 个人简介

## 基本信息

* 徐晨
* 男
* 工作经验: 3年
* 山东管理学院(计算机应用 2014届)
* [Github](https://github.com/free1)    
* [个人博客](http://free1.github.io/)
* Email: 747549945@qq.com, freeloverails@gmail.com   


## 后端
* 熟悉ruby相关生态圈，熟练掌握ruby语法和rails框架的用法, 了解ruby语言和rails源码
* 熟悉常用gem, [Github上star的项目](https://github.com/stars/free1)基本都用过了, 大部分理解原理
* api和网站后台的业务逻辑都没问题, 能独立解决遇到的所有问题, 对框架性能优化也有所掌握, 熟练部署rails应用, rails核心代码也有所了解, 会写基本rspec测试
* 熟悉web安全及http协议, 熟悉MVC, 微服务, 队列消息, restfulApi等架构
* 熟悉linux常用命令, 会使用shell命令
* 了解mysql运行机制, 体系架构, 存储引擎, 熟练使用原生sql语句, 熟悉mysql, redis常用命令, 掌握基本性能优化
* 熟悉git, elasticsearch, websocket, postgres等
* 了解java语法和常见设计模式, 了解spring等框架使用, 了解多线程编程
* 经常使用nodejs相关工具，了解其相关生态


## 前端      
* 熟悉基本javascript语法，html和css的使用
* 会使用jquery和react相关生态(react-route等), 对angular有所了解, 能大部分完成前端与后台的数据交互, 能完成大部分的前端效果, 了解jquery源码及react原理
* 熟悉常用的前端自动化工具, ES6, bootstrap, amazeui, foundation, scss等; 用过coffeescript，slim等


## 区块链
* 简单了解比特币底层运行原理，比如p2p网络，加密验证，UTXO，工作量证明等
* 了解以太坊相关生态，使用过以太坊的web3.js和geth，简单了解智能合约的编写
* 了解闪电网络，侧链，隔离见证等增强比特币网络相关理论
* 能基本看懂貔貅(云币交易所)相关代码，并完成部署上线


## 工作经历

* [婚礼纪](http://hunliji.com/)(2014.02-2015.06)
主要负责主站web端及后端功能开发，有时客串api开发，经历了项目从开始搭建到用户接近百万     

本项目从使用grape + entity + swagger作为api架构; 使用redis作为缓存及热数据存储; sidekiq作为消息队列处理系统及用户消息和延迟任务; 数据库主要以mysql为主; 使用websocket作为即时聊天系统; 部署使用mina; 使用七牛处理资源上传及视频转码; 登录系统和权限控制没有使用相关gem; 第三方登录使用oauth2及相关gem; 定时任务使用whenever; 购物模块主要使用状态机aasm和记录跟踪paper_trail来记录订单变化; 消息推送，发送邮箱及短信，性能监控，支付等都是用的第三方服务。这个项目除了没有使用专业的搜索功能外，其他电商常见功能应该都基本使用了相关技术，因为前端不会rails还要把前端文件放入项目并添加前后端交互，我主要负责大部分开发工作，服务器维护相关工作接触较少(其他同事负责)，后来聊天系统单独一个项目加入了mongodb由其他同事维护。


* [每刻风物](https://www.makehave.com/)(2015.7-2016.8和朋友的创业项目已破产)
主要负责api及后端功能开发   

这个项目和上面的项目技术栈类似，不过api架构换成了rails_metal + jbuilder，部署换成了capistrano，支付使用的activemerchant，admin使用了devise和cancnacan及simple_form，加入了karafka定时同步抓取数据(其他同事负责)和elasticsearch用于商品搜索，加入elk用于日志分析。我主要负责大部分项目的开发及维护，数据抓取由其他同事负责。这个公司还有其他的小项目后端也都是由我负责，技术栈和主项目都差不多。这个项目一直没有多少用户基本都是业务代码。


* [粉雪科技](http://fenxuekeji.com/)(2016.08-至今)

本项目使用rails5搭建，主要以api为主，使用postgres作为主数据库，mysql作为轨迹数据库(轨迹数据量过大，采用分表按日期方式存储)，使用puma作为应用服务器，mina自动化部署，devise作为登录功能，upyun作为资源管理，爬虫自动抓取文章雪场资源，redis作为数据缓存机制等。


## 其他

* 喜欢关注各种新技术及其原理
* 能独立完成绝大部分功能需求
* 熟悉电商流程及功能开发
* 熟悉所有常见功能开发(比如cms,企业系统等)
* 熟悉常见开发模式及重构技巧
* 炒过币，挖过矿，能基本看懂各种ico白皮书
* github上更新较少, 但是gitlab的私有仓库更新较多(业余时间会freelander提升自己)
* 全栈小菜鸟, 但战斗力还可以
* 喜欢简洁规范的代码风格
* 喜欢接触新的东西, 并自认为善于学习它们
* 经常关注互联网动态
* 有想法, 一直想努力做出一番事业
* 爱好广泛, 喜欢足球, 游戏, 动漫, 电影, 经济学等~
